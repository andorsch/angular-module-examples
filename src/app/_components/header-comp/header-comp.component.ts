import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-header-comp',
  templateUrl: './header-comp.component.html',
  styleUrls: ['./header-comp.component.scss']
})
export class HeaderCompComponent implements OnInit {

  constructor(private authService: AuthService, public userService: UserService) { }


  logInOrOut() {
    if(this.userService.user) {
      this.authService.logout()
    } else {
      this.authService.login()
    }
  }

  ngOnInit(): void {
  }

}
