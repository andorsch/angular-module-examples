export interface UserModel {
    id: number,
    name: string;
    token: string;
}