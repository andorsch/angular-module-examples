import { TestService } from './_services/test.service';
import { LoaderService } from './_services/loader.service';
import { AuthService } from './_services/auth.service';
import { AuthGuard } from './_guards/auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstCompComponent } from './_components/first-comp/first-comp.component';
import { HeaderCompComponent } from './_components/header-comp/header-comp.component';
import { UserService } from './_services/user.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from './_interceptor/request.interceptor';
import { HttpService } from './_services/http.service';
import { ErrorInterceptor } from './_interceptor/error.interceptor';
import { LoaderInterceptor } from './_interceptor/loader.interceptor';
import { SpinnerComponent } from './_components/spinner/spinner.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TestModule } from './modules/test/test.module';

@NgModule({
  declarations: [
    AppComponent,
    FirstCompComponent,
    HeaderCompComponent,
    SpinnerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    
  ],
  providers: [
    HttpService,
    AuthGuard,
    AuthService,
    UserService,
    LoaderService,
    TestService,
    // The order matters 
    // HTTP_INTERCEPTORS are provided in the same order as desired for request processing
    // and in the reverse order as desired for response processing
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
