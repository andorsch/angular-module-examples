import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { TestService } from 'src/app/_services/test.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  constructor(private testService: TestService) { }

  ip: string

  getIP() {
    this.testService.getIp().subscribe(response => {
      this.ip = response.origin;
    })
  }

  getAll() {
    forkJoin([
      this.testService.getIp(),
      this.testService.getHeaders(),
      this.testService.getUserAgent()
    ]).subscribe(([ip, headers, userAgent]) => {
      console.log(ip);
      console.log(headers);
      console.log(userAgent)
    })
  }

  ngOnInit(): void {
  }

}
