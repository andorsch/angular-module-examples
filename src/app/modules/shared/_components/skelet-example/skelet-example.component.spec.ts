import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkeletExampleComponent } from './skelet-example.component';

describe('SkeletExampleComponent', () => {
  let component: SkeletExampleComponent;
  let fixture: ComponentFixture<SkeletExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkeletExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkeletExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
