import { UserModel } from './../../../../_models/user.model';
import { Component } from '@angular/core';

@Component({
  selector: 'app-built-in-directives',
  templateUrl: './built-in-directives.component.html',
  styleUrls: ['./built-in-directives.component.scss']
})
export class BuiltInDirectivesComponent {

  constructor() { }

  isAvailable: boolean;
  height: number;
  user: UserModel = {} as UserModel;
  list: Array<any> = [];
  number: number = 3;

  log(){
    console.log(this.user)
  }

}
