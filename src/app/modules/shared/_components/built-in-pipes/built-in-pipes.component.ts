import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-built-in-pipes',
  templateUrl: './built-in-pipes.component.html',
  styleUrls: ['./built-in-pipes.component.scss']
})
export class BuiltInPipesComponent implements OnInit {

  constructor() { }

  birthday =  new Date();

  text = "all of my letters are written with capital letters"

  ngOnInit(): void {
  }

}
