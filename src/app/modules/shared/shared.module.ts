import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HelpComponent } from './_components/help/help.component';
import { AttributeStyleClassBindingComponent } from './_components/attribute-style-class-binding/attribute-style-class-binding.component';
import { BuiltInDirectivesComponent } from './_components/built-in-directives/built-in-directives.component';
import { CustomDirectiveComponent } from './_components/custom-directive/custom-directive.component';
import { HighlightDirective } from './_directives/highlight.directive';
import { BuiltInPipesComponent } from './_components/built-in-pipes/built-in-pipes.component';
import { CustomPipeComponent } from './_components/custom-pipe/custom-pipe.component';
import { FirstLetterPipe } from './_pipes/first-letter.pipe';
import { SkeletExampleComponent } from './_components/skelet-example/skelet-example.component';
import { TestComponent } from './_components/test/test.component';
import { TestUserComponent } from './_components/test-user/test-user.component';

@NgModule({
  declarations: [
    HelpComponent,
    AttributeStyleClassBindingComponent,
    BuiltInDirectivesComponent,
    CustomDirectiveComponent,
    HighlightDirective,
    BuiltInPipesComponent,
    CustomPipeComponent,
    FirstLetterPipe,
    SkeletExampleComponent,
    TestComponent,
    TestUserComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    HelpComponent,
    AttributeStyleClassBindingComponent,
    BuiltInDirectivesComponent,
    CustomDirectiveComponent,
    BuiltInPipesComponent,
    CustomPipeComponent,
    SkeletExampleComponent
  ]
})
export class SharedModule { }
