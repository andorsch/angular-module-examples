import { Component, OnInit, ViewChild } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  
  constructor() { 
   console.log(this.test)
  }

  @ViewChild('childComponent') childComponent: ChildComponent;

  test = "test";

  // It's executed when the component loads (after then constructor is executed)
  ngOnInit(): void {
  
  }
  
  // It's called after Angular has fully initialized a component's view
  ngAfterViewInit(): void {

    this.childComponent.onUpdate.subscribe(()=>{

    });
    
    setTimeout(()=> {
      this.test = "Updatat"
      this.childComponent.input2 = "newUserID"
    }, 2000)
  }

  // It's called when the components gets destroyed
  ngOnDestroy(): void {

  }

  seeUpdate() {
    console.log('Child updated')
    console.log(this.test)
  }

}
