import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent {

  constructor() { }

  @Input() input1

  @Input() twoWay

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
  }

  // Use ngOnChanges or setter for input 

  private _input2
  @Input() set input2(value: any) {
    if (value) {
      this._input2 = value;
      //DO something after
      // for ex:
      console.log(value)
    }
  }

  get input2() {
    return this._input2
  }

  @Output() onUpdate = new EventEmitter();

  @Output() twoWayChange = new EventEmitter();

  update() {
    this.twoWay = "updated";
    this.twoWayChange.emit(this.twoWay);

    this.onUpdate.emit();
  }

  ngOnDestroy() {
  }

}