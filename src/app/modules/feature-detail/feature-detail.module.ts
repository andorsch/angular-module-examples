import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailComponent } from './_components/detail/detail.component';
import { SharedModule } from '../shared/shared.module';
import { FeatureRoutingDetailModule } from './feature-detail-routing.module';

@NgModule({
  declarations: [DetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    FeatureRoutingDetailModule
  ]
})
export class FeatureDetailModule { }
