import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  constructor() { }

  date = new Date();

  refresh() {
    this.date = new Date();
  }

  ngOnInit(): void {
  }

}
