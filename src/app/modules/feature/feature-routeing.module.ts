import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SummaryComponent } from './_components/summary/summary.component';

const routes: Routes = [
    { path: '', component: SummaryComponent},
    { path: 'Detail', loadChildren: () => import('../feature-detail/feature-detail.module').then(m => m.FeatureDetailModule) }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FeatureRoutingModule { }
