import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummaryComponent } from './_components/summary/summary.component';
import { SharedModule } from '../shared/shared.module';
import { FeatureRoutingModule } from './feature-routeing.module';

@NgModule({
  declarations: [SummaryComponent],
  imports: [
    CommonModule,
    FeatureRoutingModule,
    SharedModule
  ]
})
export class FeatureModule { }
