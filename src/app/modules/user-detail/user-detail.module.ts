import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailsComponent } from './_components/user-details/user-details.component';
import { UserRoutingModule } from './user-detail-routeing.module';



@NgModule({
  declarations: [UserDetailsComponent],
  imports: [
    CommonModule,
    UserRoutingModule
  ]
})
export class UserDetailModule { }
