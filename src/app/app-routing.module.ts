import { AuthGuard } from './_guards/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstCompComponent } from './_components/first-comp/first-comp.component';

const routes: Routes = [
  { path: '', component: FirstCompComponent },

  { path: 'Test', loadChildren: () => import('./modules/test/test.module').then(m => m.TestModule)},

  { path: 'Home', loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule) },

  { path: 'Feature', loadChildren: () => import('./modules/feature/feature.module').then(m => m.FeatureModule) },

  { path: 'UserDetail/:id', loadChildren: () => import('./modules/user-detail/user-detail.module').then(m => m.UserDetailModule), canActivate: [AuthGuard] },

  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
