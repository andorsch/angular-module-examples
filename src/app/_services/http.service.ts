import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) {

  }

  getRequest<T>(path: string) {
    return this.http.get<T>(path)
  }

  sendRequest<T>(path: string, request: any = {}) {
    return this.http.post<T>(path, request)
  }

}