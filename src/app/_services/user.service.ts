import { UserModel } from './../_models/user.model';
import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private httpService: HttpService) {
        console.log('UserService')
    }

    user: UserModel;

    getUserDetails() {
       return this.httpService.getRequest<UserModel>('/GetUserDetails' ).pipe(tap(response => {
            this.user = response;
        }))
      
    }

}