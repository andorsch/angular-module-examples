
import { UserModel } from './../_models/user.model';
import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
    providedIn: 'root'
})
export class TestService {

    constructor(private httpService: HttpService) {
    }


    getIp() {
        return this.httpService.getRequest<{ origin: string }>('/ip')
    }

    getUserAgent() {
        return this.httpService.getRequest<{ "user-agent": string }>('/user-agent')
    }

    getHeaders() {
        return this.httpService.getRequest<{ headers: string }>('/headers')
    }

}






