import { Injectable } from '@angular/core';
import { UserModel } from '../_models/user.model';
import { UserService } from './user.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private userService: UserService) { }

    token: string;

    login() {
        // Do login request here
        this.userService.user = {
            id: 1,
            name: 'Andor',
            token: 'test'
        } as UserModel;
        this.token = this.userService.user.token;
        
    }

    logout() {
        this.userService.user = null;
    }
}